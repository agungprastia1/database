-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 31, 2021 at 09:51 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(15) NOT NULL,
  `id_card_number` int(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `place_of_bird` varchar(255) NOT NULL,
  `date_of_bird` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `gender` varchar(1) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `id_card_number`, `name`, `place_of_bird`, `date_of_bird`, `gender`, `address`) VALUES
(1, 11323, 'Jimmy', 'Malang', '2020-05-07 17:00:00', 'L', 'Jl Magelang 01'),
(2, 67565, 'Fallen', 'Jakarta', '2020-05-07 17:00:00', 'P', 'Jl Tembus 002'),
(3, 20030, 'Jaka', 'Surabaya', '2020-05-07 17:00:00', 'L', 'Gang Gabus jalan utama');

-- --------------------------------------------------------

--
-- Table structure for table `price_room_category`
--

CREATE TABLE `price_room_category` (
  `id` int(10) NOT NULL,
  `room_category_id` int(15) NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `price_room_category`
--

INSERT INTO `price_room_category` (`id`, `room_category_id`, `price`, `discount`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 500000, 0, 'admin', '2020-05-08 06:06:09', 'admin', '2020-05-08 06:06:20'),
(2, 2, 350000, 0, 'admin', '2020-05-08 06:06:11', 'admin', '2020-05-08 06:06:23'),
(3, 3, 275000, 0, 'admin', '2020-05-08 06:06:12', 'admin', '2020-05-08 06:06:24');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `room_category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_by` varchar(15) NOT NULL,
  `ctreated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `room_category`
--

CREATE TABLE `room_category` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` double NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_category`
--

INSERT INTO `room_category` (`id`, `name`, `description`, `active`, `created_by`, `created_at`) VALUES
(1, 'Large', 'size >= 7x7', 1, 'admin', '2020-05-08 05:41:15'),
(2, 'Medium', 'size >= 5.5x5.5', 1, 'admin', '2020-05-08 05:41:17'),
(3, 'Small', 'size >= 4.5x4.5', 1, 'admin', '2020-05-08 05:41:20'),
(4, 'Family', 'size >= 5x7', 0, 'admin', '2020-05-08 05:42:31'),
(5, 'Exclusive', '-', 0, 'admin', '2020-05-08 05:42:33');

-- --------------------------------------------------------

--
-- Table structure for table `stock_room_category`
--

CREATE TABLE `stock_room_category` (
  `id` int(15) NOT NULL,
  `room_category_id` int(15) NOT NULL,
  `stock_total` int(15) NOT NULL,
  `stock_avalibe` int(15) NOT NULL,
  `stock_ordered` int(15) NOT NULL,
  `created_by` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_by` varchar(15) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stock_room_category`
--

INSERT INTO `stock_room_category` (`id`, `room_category_id`, `stock_total`, `stock_avalibe`, `stock_ordered`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 3, 2, 1, 'admin', '2020-05-08 06:10:01', 'admin', '2020-05-08 06:10:19'),
(2, 3, 2, 2, 0, 'admin', '2020-05-08 06:10:04', 'admin', '2020-05-08 06:10:21'),
(3, 2, 3, 0, 3, 'admin', '2020-05-08 06:10:06', 'admin', '2020-05-08 06:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` int(15) NOT NULL,
  `number` varchar(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `created_by` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_history`
--

INSERT INTO `transaction_history` (`id`, `number`, `customer_id`, `created_by`, `created_at`) VALUES
(1, 'BK001', 1, 'admin', '2020-05-08 06:12:28'),
(2, 'BK002', 3, 'admin', '2020-05-08 06:12:31'),
(3, 'BK003', 1, 'admin', '2020-05-08 06:12:33'),
(4, 'BK004', 2, 'admin', '2020-05-08 06:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history_detail`
--

CREATE TABLE `transaction_history_detail` (
  `id` int(11) NOT NULL,
  `transaction_history_id` int(15) NOT NULL,
  `room_id` int(15) NOT NULL,
  `price` int(15) NOT NULL,
  `starts` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1),
  `ends` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total_night` int(15) NOT NULL,
  `created_by` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(15) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_history_detail`
--

INSERT INTO `transaction_history_detail` (`id`, `transaction_history_id`, `room_id`, `price`, `starts`, `ends`, `total_night`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(6, 1, 1, 500000, '2020-05-08 06:35:34.9', '2020-05-09 06:35:38', 1, 'admin', '2020-05-09 06:35:38', 'admin', '2020-05-09 06:35:38'),
(7, 2, 1, 500000, '2020-05-08 06:35:34.9', '2020-05-09 06:35:38', 1, 'admin', '2020-05-09 06:35:38', 'admin', '2020-05-09 06:35:38'),
(8, 3, 1, 500000, '2020-05-08 06:35:34.9', '2020-05-09 06:35:38', 1, 'admin', '2020-05-09 06:35:38', 'admin', '2020-05-09 06:35:38'),
(9, 4, 1, 500000, '2020-05-08 06:35:34.9', '2020-05-09 06:35:38', 1, 'admin', '2020-05-09 06:35:38', 'admin', '2020-05-09 06:35:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_room_category`
--
ALTER TABLE `price_room_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_category_id` (`room_category_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_category_id` (`room_category_id`);

--
-- Indexes for table `room_category`
--
ALTER TABLE `room_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_room_category`
--
ALTER TABLE `stock_room_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_category_id` (`room_category_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_history_detail`
--
ALTER TABLE `transaction_history_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transaction_history_id` (`transaction_history_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `price_room_category`
--
ALTER TABLE `price_room_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stock_room_category`
--
ALTER TABLE `stock_room_category`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaction_history_detail`
--
ALTER TABLE `transaction_history_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `price_room_category`
--
ALTER TABLE `price_room_category`
  ADD CONSTRAINT `price_room_category_ibfk_1` FOREIGN KEY (`room_category_id`) REFERENCES `room_category` (`id`);

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`room_category_id`) REFERENCES `room_category` (`id`);

--
-- Constraints for table `stock_room_category`
--
ALTER TABLE `stock_room_category`
  ADD CONSTRAINT `stock_room_category_ibfk_1` FOREIGN KEY (`room_category_id`) REFERENCES `room_category` (`id`);

--
-- Constraints for table `transaction_history_detail`
--
ALTER TABLE `transaction_history_detail`
  ADD CONSTRAINT `transaction_history_detail_ibfk_1` FOREIGN KEY (`transaction_history_id`) REFERENCES `transaction_history` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
